import React, { useState, useEffect}  from 'react';
import { useParams } from "react-router-dom";

const User = () => {
  let params = useParams();

  return (
    <>
        <h1>User detail</h1> 
        <h2>User: {params.id}</h2>
    </>
  )
}

export default User;