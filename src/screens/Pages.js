import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './Home';
import User from './Users';

const Pages = () => {
    return(
        <Router>
            <Routes>
                <Route path="/" element={<Home />}></Route>
                <Route path="/user/:id" element={<User />}></Route>
            </Routes>
        </Router>
    );
};

export default Pages;
